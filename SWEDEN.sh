#!/bin/bash

cat << "Startup"


   d888888o.  `8.`888b                 ,8' 8 8888888888   8 888888888o.      8 8888888888   b.             8
 .`8888:' `88. `8.`888b               ,8'  8 8888         8 8888    `^888.   8 8888         888o.          8
 8.`8888.   Y8  `8.`888b             ,8'   8 8888         8 8888        `88. 8 8888         Y88888o.       8
 `8.`8888.       `8.`888b     .b    ,8'    8 8888         8 8888         `88 8 8888         .`Y888888o.    8
  `8.`8888.       `8.`888b    88b  ,8'     8 888888888888 8 8888          88 8 888888888888 8o. `Y888888o. 8
   `8.`8888.       `8.`888b .`888b,8'      8 8888         8 8888          88 8 8888         8`Y8o. `Y88888o8
    `8.`8888.       `8.`888b8.`8888'       8 8888         8 8888         ,88 8 8888         8   `Y8o. `Y8888
8b   `8.`8888.       `8.`888`8.`88'        8 8888         8 8888        ,88' 8 8888         8      `Y8o. `Y8
`8b.  ;8.`8888        `8.`8' `8,`'         8 8888         8 8888    ,o88P'   8 8888         8         `Y8o.`
 `Y8888P ,88P'         `8.`   `8'          8 888888888888 8 888888888P'      8 888888888888 8            `Yo

Startup

echo "Welcome to SWEDEN!"
echo "Working title: Sure, We'll Efficiently Download Everything New"
echo "The script will begin shortly."
sleep 3

echo Updating SWEDEN with changes made to upstream.
git fetch --all
git reset --hard origin/master
git pull

echo "Please specify the path to your flags directory."
echo "Example: /home/user/Pictures/Flags"

read flagDirectory

echo "The flag directory you input is: "$flagDirectory"."
echo "If this does not appear correct, please exit the script and start again."
sleep 3

function validate_url(){
    if [[ `wget -S --spider $1 2>&1 | grep 'HTTP/1.1 200 OK'` ]]; then
        return 0
    else
        return 1
    fi
}

remoteRepo=https://gitlab.com/flagtism/Extra-Flags-for-4chan/-/raw/master/flags/
echo $remoteRepo
cd $flagDirectory
pwd

mapfile -t mainArray < <(find . -mindepth 0 -maxdepth 10 -type d -exec bash -c 'cd "$0" && pwd' {} \;)
flagsNotFoundArr=()

substrSize=${#flagDirectory}

for folder in "${mainArray[@]}"
do
    cd "$folder"
    currentDir=`pwd`
    echo "$currentDir"
    # Store all found flags into a new array, for comparison purposes against remote repo
    mapfile -t flagArray < <(find . -maxdepth 1 -type f -name "*.png" | cut -c 3-)

    # Once all flags in folder have been compared, clear array and repeat with new folder
    for file in "${flagArray[@]}"
    do
        echo Updating "$file"...
        substr=$(echo ${currentDir:$substrSize})
        URL=$(echo $remoteRepo$substr/$file)
        updatedURL=$(echo "$URL" | sed -e 's/ /%20/g')
	echo $updatedURL
        if `validate_url $updatedURL`; then
            wget -O "$file" "$updatedURL"
            echo "$file updated!"
            flagsFound=$((flagsFound+1))
        else
            echo "$file not found on remote repo!"
	    flagsNotFoundArr+=( "$file" )
            flagsNotFound=$((flagsNotFound+1))
        fi
    done
done

echo $flagsFound flags have been updated!
echo $flagsNotFound flags were not found in the repo - please ensure your folders/files follow the same naming schema as those on the repo.

while true; do
	read -p "Would you like to see a list of flags that weren't updated? " promptOne
	case $promptOne in
		[Yy]* ) showNotFound=$((showNotFound=1)); break;;
		[Nn]* ) showNotFound=$((showNowFound=0)); break;;
		* ) echo "Please answer yes (Y) or no (N).";;
	esac
done

printf '%s\n' "${flagsNotFoundArr[@]}"

echo Hint: There is a good chance recently-updated regions have had minor changes to their names!



# Add support for downloading files which are also folders (e.g. "Hedemora Municipality.png") - TO DO

# BONUS:
# Ignore certain directories (e.g. 1Map and 1Script)
# Only download remote flag if difference is detected between it and local copy (perhaps compare file sizes?)
