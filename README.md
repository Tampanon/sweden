# SWEDEN

SWEDEN, or **S**ure, **W**e'll **E**fficiently **D**ownload **E**verything **N**ew, is a simple bash script for updating your local flag collection with changes made upstream to the Extra-Flags-for-4chan repo. The primary purpose of this script is to automate the time-intensive task of manually updating your collection when a region receives a new/updated flag within the official repository. This project (and its title) were inspired in-part by the recent overhaul of Sweden's flag.

[insert cool image here in the future]

# Requirements

If you need help meeting any of these requirements, feel free to ask on /flag/ or open an issue on the repo.

*  Ability to run bash files. Linux or MacOS is recommended, but it's still easily doable on Windows.
*  [git](https://git-scm.com/) is required for (future) version control and automatic updates.
*  [Imagemagick](https://imagemagick.org/index.php) must be installed, or the script won't be able to interface with images.

# Usage

1.  Clone this directory to a safe location on your computer using the following terminal command: `git clone https://gitlab.com/Tampanon/SWEDEN`
2.  Enter the cloned directory `cd SWEDEN`
3.  Make the script executable `chmod +x SWEDEN.sh`
4.  Run the script `./SWEDEN.sh`
5.  When prompted, enter the directory path to your "Flags" folder, *e.g. /home/user/Pictures/Flags* **(I highly recommend making a backup of your flags, just in-case).**
6.  Wait for the script to finish. If running the script on your entire flags directory, this could take a while. You may have better luck running it on a single country, or leaving the script to run for a few hours if you'd prefer to do it all-at-once.

# Troubleshooting

*Flags inside of a folder with the same name aren't updated!*
*  This isn't exactly the case, but it does seem that way to an end user. The primary reason is because this script was built-around the folder structure found at the Extra-Flags-for-4chan repository. If your folder structure is "United States/Florida/Florida.png", "Florida.png" will not be updated. Your folder structure in this case should be "United States/Florida.png". I plan on releasing an update to support non-repo-compliant folder structures in the future.

*This script destroyed some of my flags!*
*  This shouldn't happen. If it does, I warned you to make a backup. I recommend for example copying your "Sweden" directory to a new folder, and running the script on that directory. When it finishes, double-check the resulting flags, and copy the flags back to your original "Sweden" directory.

*Some folders aren't found, no matter what!*
*  Your folders need to be named exactly the same as they are on the git repo, including all special characters and accent-marks.